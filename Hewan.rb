class Hewan
	
	attr_accessor :kaki,:alat_nafas


	def initialize(kaki, alat_nafas)
		@kaki = kaki 
		@alat_nafas = alat_nafas
	end
	

	def kaki
		@kaki
	end

	def kaki=(kaki)
		@kaki = kaki
	end	
	
	def alat_nafas
		@alat_nafas
	end

	def alat_nafas=(alat_nafas)
		@alat_nafas = alat_nafas
	end

	def info
		puts "#{self.class.name} memiliki #{kaki} kaki bernafas dengan #{alat_nafas}"
	end
	
	
end

class Kucing < Hewan

	attr_accessor :kaki,:alat_nafas

	def initialize(kaki,alat_nafas)
		 
		super(kaki,alat_nafas)
			
			
	end

		
			
end 

class Ayam < Hewan
	
	attr_accessor :kaki,:alat_nafas

	def initialize(kaki,alat_nafas)

		super(kaki,alat_nafas)
		
	end	
	

	
				
	
	
end

class Ikan < Hewan

	attr_accessor :kaki,:alat_nafas

	def initialize(kaki,alat_nafas)
		
		@kaki=kaki
		@alat_nafas=alat_nafas
		
	end

	def ket_kaki
		puts "ikan tidak memiliki kaki"
	end	

	def ket_an
		puts "ikan bernafas dengan #{alat_nafas} daan hidup di air"
	end	
	
end


kuc = Kucing.new('4','paru')
kuc.info


aym = Ayam.new('2','paru')
aym.info

fish = Ikan.new('0','Insang')
fish.info
fish.ket_kaki
fish.ket_an
